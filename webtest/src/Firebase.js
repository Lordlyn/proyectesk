import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getFirestore } from "firebase/firestore";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
    apiKey: "AIzaSyC7WELMjV0xx6EH7lqpcrAUmJYL4JR5sOs",
    authDomain: "proyectd-24af5.firebaseapp.com",
    projectId: "proyectd-24af5",
    storageBucket: "proyectd-24af5.appspot.com",
    messagingSenderId: "854672782164",
    appId: "1:854672782164:web:4127a6613010c3b46f0ec1",
    measurementId: "G-VB73499SNR"
  };

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const auth = getAuth(app);
export const firestoreDB = getFirestore(app);
