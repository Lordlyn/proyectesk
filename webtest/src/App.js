import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import imgLogin from './images/soulb.png'
//FIREBASE
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth';
import { doc, setDoc } from "firebase/firestore"
import { auth, firestoreDB } from './Firebase';

function App() {
  const [email, setEmailUser] = useState('')
  const [nameUser, setNameUser] = useState('')
  const [password, setPassword] = useState('')
  const [phoneNumber, setPhonenumber] = useState('')
  const [gender, setGender] = useState('')

  // Verificacion de datos
  const [passwordError, setPasswordError] = useState(false)
  
  const onChangeUser = (e) => {
    setNameUser(e.target.value, () => {
      console.log(nameUser);
    });
  }

  useEffect(() => {
    console.log(nameUser);
  }, [nameUser]);

  useEffect(() => {
    console.log(password);
  }, [password]);

  const onChangePassword = (e) => {
    setPassword(e.target.value);
    console.log(password);
  }

  const onPhoneNumber = (e) => {
    setPhonenumber(e.target.value);
    console.log(phoneNumber);
  }

  const onGenderSelect = (e) => {
    setGender(e.target.value);
    console.log(gender);
  }

  const onEmailUser = (e) => {
    setEmailUser(e.target.value);
    console.log(email);
  }
  
  const getWelcomeMessage = (e) => {
    if (gender === 'Masculino') {
      return '!Bienvenido!';
    } else if (gender === 'Femenino') {
      return '¡Bienvenida!';
    } else {
      return '¡Bienvenido/a!'
    }
  }

  const SignIn = async () => {
    if (!nameUser || !email || !password || !gender) {
      alert("Por favor complete todos los campos requeridos.")
      return;
    }
    try {
      await signInWithEmailAndPassword(auth, email, password);
      alert(getWelcomeMessage());
    } catch (error) {
      console.error(error);
      alert('Error al iniciar sesión:' + error.message);
      setPasswordError(true);
    }
  }

  const UserRegister = async () => {
    if (!nameUser || !email || !password || !gender) {
      alert("Por favor complete todos los campos requeridos.")
      return;
    }
    try {
      await createUserWithEmailAndPassword(auth, email, password);
      alert('¡Registro exitoso!');
      SavetoFirestoreDBData();
    } catch (error) {
      console.error(error);    
      alert('Error al registrar: ' + error.message);
      setPasswordError(true);
    }
  };

  const SavetoFirestoreDBData = async () => {
    try {
      const userRef = doc(firestoreDB, "users", nameUser);
      const userData = {
        Nombre: nameUser,
        Ciudad: "Villavicencio",
        País: "Colombia",
        Email: email,
        Phone: phoneNumber,
        Género: gender,
      };
      await setDoc(userRef, userData);
    } catch (error) {
      console.error(error);
      alert('Error al guardar información de usuario: ' + error.message);
    }
  };

    
  return (
    <DivAppCSS className="App">
      <div className="sectionLogin">
        <div className="divImgLogin">
          <img src={imgLogin} alt="Logo" />
          <input value={nameUser} onChange={(e) => onChangeUser(e)} placeholder="Nombre de usuario"/>
          <input value={email} onChange={(e) => onEmailUser(e)} placeholder='Correo Electrónico'/>
          <input type="password" value={password} onChange={(e) => onChangePassword(e)} placeholder="Contraseña"/>
          <input value={phoneNumber} onChange={(e) => onPhoneNumber(e)} placeholder="Telefono"/>
          <label>
            Género:
              <select value={gender} onChange={(e) => onGenderSelect(e)}>
                <option value="">Seleccione...</option>
                <option value="Masculino">Masculino</option>
                <option value="Femenino">Femenino</option>
                <option value="Otro">Otro</option>
      </select>
  </label>
          <button onClick={() => SignIn()}>Iniciar sesión</button>
          <button onClick={() => UserRegister()}>Registrarse</button>
          {passwordError && <p>Contraseña incorrecta. Inténtalo de nuevo.</p>}
      </div>
    </div>
  </DivAppCSS>
  );
};


const DivAppCSS = styled.div`

  background-color: #424242;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;

  .sectionLogin {
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #cfd8dc;
    width: 43%;
    height: 91%;
    border-radius: 3px;

    .divImgLogin {
      margin-bottom: 85px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      width: 60%;

      input {
        width: 200px;
        height: 30px;
        margin: 17px 2px;
        border-radius:0.5em;
        overflow: hidden;
        box-shadow: 0 4px 6px -5px hsl(0, 0%, 60%), inset 0px 4px 6px -5px black;
      }

      img {
        width: 100%;
      }

      button {
        margin-top: 20px;
        background-color: #faebd7;
        color: #080808;
        border: none;
        box-shadow: 2px 2px 3px 1px #afeeee;
        border-radius: 10px;
        padding: 11px;
        width: 180px;
      }
    }
  }
`;



export default App;